export const SIGN_UP = 'SIGN_UP';
export const SIGN_IN = 'SIGN_IN';
export const LOAD_USER = 'LOAD_USER';
export const LOG_OUT = 'LOG_OUT';
export const STOP_INITIAL_LOADER = 'STOP_INITIAL_LOADER';
export const AUTH_ERROR = 'AUTH_ERROR';

// ALerts
export const ADD_ALERTS = 'ADD_ALERTS';
export const CLOSE_ALERT = 'CLOSE_ALERT';
export const CLEAR_ALERTS = 'CLEAR_ALERTS';
