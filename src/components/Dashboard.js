import React from 'react';
import AppLayout from '../Layouts';
import { Card, Button, Typography, Space } from 'antd';
import { LoginOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';
import { logOut } from '../store/actions/auth';

const { Text } = Typography;
const Dashboard = ({ authState: { user }, logOut, ...rest }) => {
	return (
		<AppLayout>
			<Card title='Dashboard' style={{ width: 450, margin: 'auto' }}>
				<Space direction='vertical'>
					<Text>Name: {user && user.name}</Text>
					<Text>Username: {user && user.username}</Text>
					<Text>Email Address: {user && user.email}</Text>
					<Text>Id: {user && user._id}</Text>
				</Space>
				<div style={{ textAlign: 'center', marginTop: '20px' }}>
					<Button onClick={logOut} danger icon={<LoginOutlined />}>
						Log out
					</Button>
				</div>
			</Card>
		</AppLayout>
	);
};

const mapStateToProps = (state) => ({
	authState: state.AUTH_STATE,
});

export default connect(mapStateToProps, { logOut })(Dashboard);
