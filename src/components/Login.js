import React, { useState, useEffect } from 'react';
import AppLayout from '../Layouts';
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Card } from 'antd';
import { Typography } from 'antd';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { signInUser } from '../store/actions/auth';

const { Text } = Typography;
const Login = ({ authState: { isAuthenticated }, signInUser, ...rest }) => {
	const history = useHistory();
	const [submitted, setSubmitted] = useState(false);

	useEffect(() => {
		if (isAuthenticated) {
			history.push('/dashboard');
		}
		// eslint-disable-next-line
	}, [isAuthenticated]);

	const onFinish = async (values) => {
		setSubmitted(true);
		const isSuccess = await signInUser(values.username, values.password);
		if (!isSuccess) {
			setSubmitted(false);
		}
	};

	return (
		<AppLayout>
			<Card title='Sign In' style={{ width: 450, margin: 'auto' }}>
				<Form
					name='normal_login'
					className='login-form'
					initialValues={{ remember: true }}
					onFinish={onFinish}
				>
					<Form.Item
						name='username'
						rules={[{ required: true, message: 'Please input your Username!' }]}
					>
						<Input
							prefix={<UserOutlined className='site-form-item-icon' />}
							placeholder='Username'
						/>
					</Form.Item>
					<Form.Item
						name='password'
						rules={[{ required: true, message: 'Please input your Password!' }]}
					>
						<Input
							prefix={<LockOutlined className='site-form-item-icon' />}
							type='password'
							placeholder='Password'
						/>
					</Form.Item>

					<Form.Item style={{ marginTop: '20px', textAlign: 'center' }}>
						<Button
							type='primary'
							htmlType='submit'
							className='login-form-button'
							loading={submitted}
						>
							{submitted ? 'Loading' : 'Log in'}
						</Button>
					</Form.Item>
					<Text>
						Don't have an Account? <Link to='/register'>Register Here </Link>
					</Text>
				</Form>
			</Card>
		</AppLayout>
	);
};

const mapStateToProps = (state) => ({
	authState: state.AUTH_STATE,
});

export default connect(mapStateToProps, { signInUser })(Login);
