import React, { useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { loadUser } from './store/actions/auth';

// Components
import Login from './components/Login';
import Dashboard from './components/Dashboard';

// Route
import PrivateRoute from './routing/PrivateRoute';

function App({ loadUser }) {
	// Call load user on first load
	useEffect(() => {
		loadUser();
		//eslint-disable-next-line
	}, []);

	return (
		<Router>
			<Switch>
				<Route exact path='/' component={Login} />
				<PrivateRoute exact path='/dashboard' component={Dashboard} />
			</Switch>
		</Router>
	);
}

export default connect(null, { loadUser })(App);
