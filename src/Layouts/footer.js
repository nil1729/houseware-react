import { Footer } from 'antd/lib/layout/layout';
import React from 'react';

export default function AppFooter() {
	return <Footer style={{ textAlign: 'center' }}>Houseware © 2021</Footer>;
}
