import React, { useEffect } from 'react';
import './styles.css';
import { Layout, message, Skeleton } from 'antd';
import AppHeader from './header';
import AppFooter from './footer';
import { connect } from 'react-redux';

const { Content } = Layout;

const openNotificationWithIcon = (type, msg) => {
	message[type](msg);
};

const AppLayout = ({ children, alertState, authState: { loading } }) => {
	useEffect(() => {
		if (alertState && alertState.message && alertState.variant) {
			openNotificationWithIcon(alertState.variant, alertState.message);
		}
	}, [alertState]);

	return (
		<Layout className='layout'>
			<AppHeader />
			{loading ? (
				<div style={{ margin: '30px 40px' }}>
					<Skeleton avatar paragraph={{ rows: 4 }} />
				</div>
			) : (
				<Content className='site-layout-content'>{children}</Content>
			)}

			<AppFooter />
		</Layout>
	);
};

const mapStateToProps = (state) => ({
	authState: state.AUTH_STATE,
	alertState: state.ALERTS,
});

export default connect(mapStateToProps)(AppLayout);
