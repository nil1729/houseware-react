import { Header } from 'antd/lib/layout/layout';
import React from 'react';

const AppHeader = () => {
	return (
		<Header>
			<div className='logo'>Houseware</div>
		</Header>
	);
};

export default AppHeader;
